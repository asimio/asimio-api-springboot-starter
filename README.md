# README #

Accompanying source code for blog entries at:

* http://tech.asimio.net/2017/06/29/Implementing-a-custom-SpringBoot-starter-for-CXF-and-Swagger.html
* http://tech.asimio.net/2018/07/12/Implementing-a-custom-Maven-Archetype-to-generate-Spring-Boot-based-services.html

### Requirements ###

* Java 8
* Maven 3.3.x

### Building the artifact(s) ###

```
mvn clean install
```

For client apps to be able to pull it from .m2 (or deploy to a Maven repo manager or https://tech.asimio.net/2018/06/27/Using-an-AWS-S3-Bucket-as-your-Maven-Repository.html)

### See also ###

https://bitbucket.org/asimio/asimio-api-starter-demo

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero