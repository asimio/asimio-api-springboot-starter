package com.asimio.api.springboot.autoconfigure;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties(prefix = AsimioApiProperties.ASIMIO_API_PREFIX)
public class AsimioApiProperties {

    public static final String ASIMIO_API_PREFIX = "asimio-api";

    @NestedConfigurationProperty
    private SwaggerDocs docs = new SwaggerDocs();

    @PostConstruct
    public void init() {
        // Safe defaults
        if (this.docs.prettyPrint == null) {
            this.docs.prettyPrint = "true";
        }
    }

    public SwaggerDocs getDocs() {
        return docs;
    }

    public void setDocs(SwaggerDocs docs) {
        this.docs = docs;
    }

    public static class SwaggerDocs {

        @Value("${cxf.path:/}")
        private String basePath;
        private String title;
        private String description;
        private String version;
        private String contact;
        private String prettyPrint;

        public String getBasePath() {
            return basePath;
        }

        public void setBasePath(String basePath) {
            this.basePath = basePath;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getContact() {
            return contact;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getPrettyPrint() {
            return prettyPrint;
        }

        public void setPrettyPrint(String prettyPrint) {
            this.prettyPrint = prettyPrint;
        }
    }
}